<?php

class Customer {

	//data for a customer

	//public static $customerCount = 0;
	

	public $id;
	public $name;
	public $email;
	public $university;
	public $address;
	public $city;
	public $state;
	public $country;
	public $zip;
	public $phone;

	function __construct($id, $name, $email, $university, $address, $city, $state, $country, $zip, $phone) {
		$this->id = $id;
		$this->name = $name;
		$this->email = $email;
		$this->university = $university;
		$this->address = $address;
		$this->city = $city;
		$this->state = $state;
		$this->country = $country;
		$this->zip = $zip;
		$this->phone = $phone;
		//self::$customerCount++;

	}



}
?>