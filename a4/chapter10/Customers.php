<?php

class Customers {

	//data for a customer

	//public static $customerCount = 0;
	

	public $id;
	public $name;
	public $email;
	public $university;
	public $city;

	function __construct($id, $name, $email, $university, $city) {
		$this->id = $id;
		$this->name = $name;
		$this->email = $email;
		$this->university = $university;
		$this->city = $city;
		//self::$customerCount++;

	}

	public function readCustomers($fileName) {

		$data = file($fileName) or die ("ERROR- cannot find the file");
		$delim = ",";
		$i = 0;

		$customerArr = array();


		//for each line in the file,  create a new instance of Customer in an array
		foreach($data as $line) {
			$fields = explode($delim, $line);

			/*
			$this->id = $fields[0];
			$this->name = $fields[1] . ' ' . $fields[2];
			$this->email = $fields[3];
			$this->university = $fields[4];
			$this->city = $fields[6];
			*/

			$customerArr[$i] = new Customers($fields[0],$fields[1] . ' ' . $fields[2], 
				$fields[3], $fields[4], $fields[6]);

			/*
			$customer = array();
			$customer['id'] = $fields[0];
			$customer['name'] = $fields[1] . ' ' . $fields[2];
			$customer['email'] = $fields[3];
			$customer['university'] = $fields[4];
			$customer['city'] = $fields[6];

			$customerArr[$customer['id']] = $customer;
			*/

		}
			return $customerArr;

	}

}
?>