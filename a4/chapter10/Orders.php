<?php


class Orders {

	public $id;
	public $customerId;
	public $isbn;
	public $title;
	public $category;

	function __construct($id, $customerId, $isbn, $title, $category) {

		$this->id = $id;
		$this->customerId = $customerId;
		$this->isbn = $isbn;
		$this->title = $title;
		$this->category = $category;



	}

	public static function readOrders($customerId, $fileName) {

		$data = file($fileName) or die ("ERROR- cannot find the file");
		$delim = ",";

		foreach($data as $line) {
			$fields = explode($delim,$line);

			$order = array();
			$order['id'] = $fields[0];
			$order['customerId'] = $fields[1];
			$order['isbn'] = $fields[2];
			$order['title'] = $fields[3];
			$order['category'] = $fields[4];

			$orders[] = $order;
		}

		foreach($orders as $currentOrder) {
			if($currentOrder['customerId'] == $customerId){
				$customerOrders[] = $currentOrder;
			}
		}

		if(isset($customerOrders)) {
			return $customerOrders;
		}else{
			return null;
		}



	}

}



?>