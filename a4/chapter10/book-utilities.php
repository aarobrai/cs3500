<?php


/*
 * read customers.txt into an array of Customer objects
 */
function readCustomers($fileName) {

   $data = file($fileName) or die ("ERROR- cannot find the file");
   $delim = ",";
   $i = 0;

   $customerArr = array();



   foreach($data as $line) {
      $fields = explode($delim, $line);

         /*
         $this->id = $fields[0];
         $this->name = $fields[1] . ' ' . $fields[2];
         $this->email = $fields[3];
         $this->university = $fields[4];
         $this->city = $fields[6];
         */

         $customerArr[$fields[0]] = new Customer($fields[0],$fields[1] . ' ' . $fields[2], 
            $fields[3], $fields[4], $fields[5], $fields[6], $fields[7], $fields[8], $fields[9], $fields[10]);

         $i++;

         /*
         $customer = array();
         $customer['id'] = $fields[0];
         $customer['name'] = $fields[1] . ' ' . $fields[2];
         $customer['email'] = $fields[3];
         $customer['university'] = $fields[4];
         $customer['city'] = $fields[6];

         $customerArr[$customer['id']] = $customer;
         */

      }
      return $customerArr;

   }


/*
 * read the data in orders.txt into an array of Order
 * objects
 */
function readOrders($customerId, $fileName) {

   $data = file($fileName) or die ("ERROR- cannot find the file");
   $delim = ",";
   $orders = array();
   $i = 0;

   foreach($data as $line) {
      $fields = explode($delim,$line);

      $orders[$i] = new Order($fields[0] , $fields[1] , $fields[2] , $fields[3] , $fields[4]);

      $i++;


      /*
      $order['id'] = $fields[0];
      $order['customerId'] = $fields[1];
      $order['isbn'] = $fields[2];
      $order['title'] = $fields[3];
      $order['category'] = $fields[4];

      $orders[] = $order;
      */

   }

   //TODO: don't know if this got broke when switching to OO

   foreach($orders as $currentOrder) {
      if($currentOrder->customerId == $customerId){
         $customerOrders[] = $currentOrder;
      }
   }

   if(isset($customerOrders)) {
      return $customerOrders;
   }else{
      return null;
   }



}

?>