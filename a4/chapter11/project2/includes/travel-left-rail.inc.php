
<div class="panel panel-default">
 <div class="panel-heading">Search</div>
 <div class="panel-body">
   <form>
     <div class="input-group">
      <input type="text" class="form-control" placeholder="search ...">
      <span class="input-group-btn">
        <button class="btn btn-warning" type="button"><span class="glyphicon glyphicon-search"></span>          
        </button>
      </span>
    </div>  
  </form>
</div>
</div>  <!-- end search panel -->       

<div class="panel panel-info">
 <div class="panel-heading">Continents</div>
 <ul class="list-group">   
   <?php
   $query = 'SELECT ContinentName FROM GeoContinents';
   $result = mysqli_query($conn,$query);

   
   while($continent = mysqli_fetch_assoc($result)) {

    //reset url to clear images, since continent links are dummy links
    echo "<li class='list-group-item'><a href=browse-images.php>".$continent['ContinentName']."</a></li>";
  }



  ?>

</ul>
</div>  <!-- end continents panel -->  
<div class="panel panel-info">
 <div class="panel-heading">Popular Countries</div>
 <ul class="list-group">  
   <?php

   $query = 'SELECT DISTINCT g.CountryName , g.ISO from GeoCountries g, TravelImageDetails t WHERE g.ISO = t.CountryCodeISO;';
   $result = mysqli_query($conn,$query);

   $link = "images/travel/square/";
   while($country = mysqli_fetch_assoc($result)) {

              //TODO: When the user clicks a country in the popular countries list, the page should behave in the same way as selecting a country from the above form
    echo "<li class='list-group-item'><a href=browse-images.php?country=". $country['ISO'].'>'.$country['CountryName']."</a></li>";
  }





  ?>

</ul>
</div>  <!-- end countries panel -->    