<?php

  //connect to mySQL database

require('includes/config.php');
$conn = mysqli_connect(DBHOST,DBUSER,DBPASS,DBNAME);

$error = mysqli_connect_error();
if($error != null) {
  $output = "<p>Database connection error</p>";
  exit($output);
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
 <title>Travel Template</title>
 <?php include 'includes/travel-head.inc.php'; ?>
</head>
<body>

  <?php include 'includes/travel-header.inc.php'; ?>

  <div class="container">  <!-- start main content container -->
   <div class="row">  <!-- start main content row -->
    <div class="col-md-3">  <!-- start left navigation rail column -->
     <?php include 'includes/travel-left-rail.inc.php'; ?>
   </div>  <!-- end left navigation rail --> 

   <div class="col-md-9">  <!-- start main content column -->
     <ol class="breadcrumb">
       <li><a href="#">Home</a></li>
       <li><a href="#">Browse</a></li>
       <li class="active">Images</li>
     </ol>          

     <div class="well well-sm">
      <form class="form-inline" role="form" method="get" action="<?php echo $_SERVER['PHP_SELF']; ?>">
        <div class="form-group" >
          <select class="form-control" name="city">
            <option value="0">Filter by City</option>
            <?php
              //display option for each country that has a matching record in travelimagedetails table
            $query = 'SELECT DISTINCT g.GeoNameID, g.AsciiName FROM GeoCities g, travelimagedetails t WHERE t.CityCode = g.geonameid;';
            $result = mysqli_query($conn,$query);
            $i = "0";
            while($city = mysqli_fetch_assoc($result)) {
              echo '<option value=';
              echo '"'.$city['GeoNameID'].'">';
              echo $city['AsciiName'] . "</option>";
            }
            ?>
          </select>
        </div>
        <div class="form-group">
          <select class="form-control" name="country">
            <option value="ZZZ">Filter by Country</option>
            <?php
             //display filtering options for popular countries
            $query = 'SELECT DISTINCT g.iso, g.CountryName from GeoCountries g, TravelImageDetails t WHERE g.ISO = t.CountryCodeISO;';
            $result = mysqli_query($conn, $query);

            while($country = mysqli_fetch_assoc($result)) {
              echo '<option value=';
              echo '"'.$country['iso'].'">';
              echo $country['CountryName'] . "</option>"; 

            }

            ?>

          </select>
        </div>  
        <button type="submit" class="btn btn-primary">Filter</button>
        <?php 

          //echo var_dump($_GET); 











        ?>
      </form>         
    </div>      <!-- end filter well -->

    <div class="well">
      <div class="row">
        <!-- display image thumbnails code here -->
        <?php

        $city = '';
        $country = '';

        if(isset($_GET['country']) || isset($_GET['city'])) {
             $query = "SELECT t.path FROM travelimage t, travelimagedetails det WHERE t.ImageID = det.ImageID";

             if(isset($_GET['country']) && strcmp($_GET['country'],'ZZZ') != 0) {
              $country = $_GET['country'];
                //echo "<p>You selected country ".$country."</p>";
              $query = $query . " AND det.CountryCodeISO = '$country'";
            }




              //if a city AND country is specified, we want only the images from that city AND country. Otherwise the user clicked a left side country button, or didn't specify a city. In that case we'll show all the images from the selected country

            if(isset($_GET['city']) && $_GET['city'] != 0) {
              $city = $_GET['city'];
                //echo "<p>You selected city ".$city."</p>";
              $query = $query . " AND det.CityCode = '$city'";
            }       

            $result = mysqli_query($conn,$query);

            while($img = mysqli_fetch_assoc($result)) {
                //echo "<p>Will display image ".$img['path']."</p>";

              echo "<div class='col-md-3'><img src = 'images/travel/square/".$img['path']."'</img></div>";
            }         
      }









      ?>
    </div>
  </div>   <!-- end images well -->

</div>  <!-- end main content column -->
</div>  <!-- end main content row -->
</div>   <!-- end main content container -->

<?php include 'includes/travel-footer.inc.php'; ?>   



 <!-- Bootstrap core JavaScript
 ================================================== -->
 <!-- Placed at the end of the document so the pages load faster -->
 <script src="bootstrap3_travelTheme/assets/js/jquery.js"></script>
 <script src="bootstrap3_travelTheme/dist/js/bootstrap.min.js"></script>
 <script src="bootstrap3_travelTheme/assets/js/holder.js"></script>
</body>
</html>