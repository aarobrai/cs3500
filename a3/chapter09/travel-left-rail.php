<div class="panel panel-default">
	<div class="panel-heading">Search</div>
	<div class="panel-body">
		<form>
			<div class="input-group">
				<input type="text" class="form-control" placeholder="search ...">
				<span class="input-group-btn">
                    <button class="btn btn-warning" type="button"><span class="glyphicon glyphicon-search"></span>
				</button>
				</span>
			</div>
		</form>
	</div>
</div>
<!-- end search panel -->

<div class="panel panel-info">
	<div class="panel-heading">Continents</div>
	<ul class="list-group">

		<?php			
		#Replace the hard-coded continents and countries lists in the file with loops 
		
			$link = "#";
		
			for($i = 0; $i < count($continents); $i++){
				echo "<li class='list-group-item'><a href=$link>".$continents[$i]."</a></li>";
			}
				
		?>
	</ul>
</div>
<!-- end continents panel -->
<div class="panel panel-info">
	<div class="panel-heading">Popular Countries</div>
	<ul class="list-group">
		
		<?php				
		
			$link = "#";
			
		
			foreach($countries as $current) {
				echo "<li class='list-group-item'><a href=$link>".$current."</a></li>";				
			}

		
		
		
		
		?>
	</ul>
</div>
<!-- end countries panel -->
