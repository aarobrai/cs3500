<?php

	$fileName = 'customers.txt';
	$customers = readCustomers($fileName);


	function readCustomers($fileName) {
		
		$data = file($fileName) or die ("ERROR- cannot find the file");
		$delim = ",";
		
		foreach($data as $line) {
			$fields = explode($delim, $line);
			
			$customer = array();
			$customer['id'] = $fields[0];
			$customer['name'] = $fields[1] . ' ' . $fields[2];
			$customer['email'] = $fields[3];
			$customer['university'] = $fields[4];
			$customer['city'] = $fields[6];
			
			$customerArr[$customer['id']] = $customer;
			
			
		}
		
		return $customerArr;
		
	}

	function readOrders($customerId, $fileName) {
		
		$data = file($fileName) or die ("ERROR- cannot find the file");
		$delim = ",";
		
		foreach($data as $line) {
			$fields = explode($delim,$line);
			
			$order = array();
			$order['id'] = $fields[0];
			$order['customerId'] = $fields[1];
			$order['isbn'] = $fields[2];
			$order['title'] = $fields[3];
			$order['category'] = $fields[4];
			
			$orders[] = $order;
		}
		
		foreach($orders as $currentOrder) {
			if($currentOrder['customerId'] == $customerId){
				$customerOrders[] = $currentOrder;
			}
		}
		
		if(isset($customerOrders)) {
			return $customerOrders;
		}else{
			return null;
		}
		
		
		
	}
?>

	<!DOCTYPE html>
	<html lang="en">

	<head>
		<meta http-equiv="Content-Type" content="text/html; 
   charset=UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="">
		<meta name="author" content="">
		<title>Book Template</title>

		<link rel="shortcut icon" href="../../assets/ico/favicon.png">

		<!-- Google fonts used in this theme  -->
		<link href='http://fonts.googleapis.com/css?family=Roboto+Slab:400,700' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic,700italic' rel='stylesheet' type='text/css'>

		<!-- Bootstrap core CSS -->
		<link href="bootstrap3_bookTheme/dist/css/bootstrap.min.css" rel="stylesheet">
		<!-- Bootstrap theme CSS -->
		<link href="bootstrap3_bookTheme/theme.css" rel="stylesheet">


		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
   <script src="bootstrap3_bookTheme/assets/js/html5shiv.js"></script>
   <script src="bootstrap3_bookTheme/assets/js/respond.min.js"></script>
   <![endif]-->
	</head>

	<body>

		<?php include 'book-header.inc.php'; ?>

		<div class="container">
			<div class="row">
				<!-- start main content row -->

				<div class="col-md-2">
					<!-- start left navigation rail column -->
					<?php include 'book-left-nav.inc.php'; ?>
				</div>
				<!-- end left navigation rail -->

				<div class="col-md-10">
					<!-- start main content column -->

					<!-- Customer panel  -->
					<div class="panel panel-danger spaceabove">
						<div class="panel-heading">
							<h4>My Customers</h4>
						</div>
						<table class="table">
							<tr>
								<th>Name</th>
								<th>Email</th>
								<th>University</th>
								<th>City</th>
							</tr>

							<!-- Read the data in customers.txt into an array, and then display the customer data in a table -->
							<?php
							
								foreach($customers as $customer) {
									echo '<tr>';
									echo '<td><a href="chapter09-project03.php?customer='. $customer['id'] . '">' .  $customer['name'] .'</a></td>';
									echo '<td>' . $customer['email'] . '</td>';
									echo '<td>' . $customer['university'] . '</td>';
									echo '<td>' . $customer['city'] . '</td>';									
									
								}		
							?>

						</table>
					</div>



					<!-- read the data in orders.txt into an array, and
					then display any matching order data... -->
					<?php					
						$fileName = "orders.txt";
						$delim = ",";
					

					
						if($_SERVER['REQUEST_METHOD'] == 'GET') {
							if(isset($_GET['customer'])) {
								$requestedCustomer = $customers[$_GET['customer']];
								
						
								echo '<div class="panel panel-danger">';
								echo '<div class="panel-heading"><h4>Orders for <strong>'. $requestedCustomer['name'] . '</strong></h4></div>';
								
								$customerOrders = readOrders($_GET['customer'], 'orders.txt');
								
								if($customerOrders === null) {
									echo '<h5>No orders for this customer</h5>';
								}else{
									
									echo "<table class='table'>";
									echo '<tr>';
									echo '<th></th>';
									echo '<th>ISBN</th>';
									echo '<th>Title</th>';
									echo '<th>Category</th>';
									echo '</tr>';
									
									
									foreach($customerOrders as $order) {
										echo '<tr>';
									
										echo "<td><img src='images/book/tinysquare/" . $order['isbn'] . ".jpg'></td>";
										echo "<td>" .$order['isbn'] . "</td>";
										//TODO: add link on book title
										echo "<td>" .$order['title'] . "</td>";
										echo "<td>" .$order['category'] . "</td>";
										echo '</tr>';
									}
									
									echo "</table>";
									echo "</div>";
								}
							}
						}	
					?>


				</div>


			</div>
			<!-- end main content column -->
		</div>
		<!-- end main content row -->
		</div>
		<!-- end container -->





		<!-- Bootstrap core JavaScript
 ================================================== -->
		<!-- Placed at the end of the document so the pages load faster -->
		<script src="bootstrap3_bookTheme/assets/js/jquery.js"></script>
		<script src="bootstrap3_bookTheme/dist/js/bootstrap.min.js"></script>
		<script src="bootstrap3_bookTheme/assets/js/holder.js"></script>
	</body>

	</html>
