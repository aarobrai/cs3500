<?php

 $link = '#';


echo "<div class='container'>
	<!-- start main content container -->
	<div class='row'>
		<!-- start main content row -->
		<div class='col-md-3'>
			<!-- start left navigation rail column -->

			<div class='panel panel-default'>
				<div class='panel-heading'>Search</div>
				<div class='panel-body'>
					<form>
						<div class='input-group'>
							<input type='text' class='form-control' placeholder='search ...'>
							<span class='input-group-btn'>
                    <button class='btn btn-warning' type='button'><span class='glyphicon glyphicon-search'></span>
							</button>
							</span>
						</div>
					</form>
				</div>
			</div>
			<!-- end search panel -->

			<div class='panel panel-info'>
				<div class='panel-heading'>Continents</div>
				<ul class='list-group'>
					<li class='list-group-item'><a href='$link'>Asia</a></li>
					<li class='list-group-item'><a href='$link'>Africa</a></li>
					<li class='list-group-item'><a href='$link'>Europe</a></li>
					<li class='list-group-item'><a href='$link'>North America</a></li>
					<li class='list-group-item'><a href='$link'>South America</a></li>
				</ul>
			</div>
			<!-- end continents panel -->
			<div class='panel panel-info'>
				<div class='panel-heading'>Popular Countries</div>
				<ul class='list-group'>
					<li class='list-group-item'><a href='$link'>Bahamas</a></li>
					<li class='list-group-item active'>Canada</li>
					<li class='list-group-item'><a href='$link'>Germany</a></li>
					<li class='list-group-item'><a href='$link'>Ghana</a></li>
					<li class='list-group-item'><a href='$link'>Greece</a></li>
					<li class='list-group-item'><a href='$link'>Hungary</a></li>
					<li class='list-group-item'><a href='$link'>Italy</a></li>
					<li class='list-group-item'><a href='$link'>Spain</a></li>
					<li class='list-group-item'><a href='$link'>United Kingdom</a></li>
					<li class='list-group-item'><a href='$link'>United States</a></li>
				</ul>
			</div>
<!-- end countries panel -->
		</div>";
	
?>
