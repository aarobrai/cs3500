<?php
	


	#echos a properly formed hyperlink 
	function generateLink($url, $class, $label) {
		echo "<a href='$url' class='$class'>$label</a>";
	}

	#echos the necessary markup for a single post
	function outputPostRow($number) {
		
		include 'travel-data.php';
		
				echo "	<div class='row'>
						<div class='col-md-2'>";
						generateLink('post.php?id='.${'userId'.$number},"", "<img src='images/travel/${'thumb' . $number}' alt='${'title'.$number}' class='img-thumbnail'");
				echo "</div>";
					
				echo "<div class='col-md-10'>
						<h2>${'title'.$number}</h2>
						<div class='details'>
							Posted by ";
	
				generateLink("user.php?id=".${'userId'.$number},"",utf8_encode(${'userName' .$number}) );
		
				echo "<span class='pull-right'>12/4/2011</span>
						</div>
					<p class='excerpt'>";
				echo ${'excerpt'.$number};
				echo "</p>
					  <p>";
						generateLink("post.php?id=".$number,"btn btn-primary btn-sm","Read More");
				echo "</p>
					</div>
				</div>";			
	}

	#use a loop to output ten page numbers that start with the value provided in $startNum
	function outputPagination($startNum, $currentNum) {
			echo "<ul class='pagination'>";
		
			for($i = $startNum; $i <= 10; $i++) {
				
				if($startNum <= 10 && $i == $startNum) {
					echo "<li class = 'disabled'>";
					generateLink("#","","&laquo");
					echo "</li>";
				}else if($i == $currentNum) {
					echo "<li class = 'active'>";
					generateLink("#","",4);
					echo "</li>";
				}else{
					echo "<li>";
					generateLink("#","",$i);
					echo "</li>";						
				}
			
			}
		
		echo "<li>";
		generateLink("#","","&raquo;");
		echo "</li>";
	}

?>

	<!DOCTYPE html>
	<html lang="en">

	<head>

		<!-- Google fonts used in this theme  -->
		<link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:700' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,800,600,700,300' rel='stylesheet' type='text/css'>

		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="">
		<meta name="author" content="">
		<link rel="shortcut icon" href="../../assets/ico/favicon.png">

		<title>Travel Template</title>



		<!-- Bootstrap core CSS -->
		<link href="bootstrap3_travelTheme/dist/css/bootstrap.min.css" rel="stylesheet">


		<!-- Custom styles for this template -->
		<link href="bootstrap3_travelTheme/theme.css" rel="stylesheet">

		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
      <script src="bootstrap3_travelTheme/assets/js/html5shiv.js"></script>
      <script src="bootstrap3_travelTheme/assets/js/respond.min.js"></script>
    <![endif]-->
	</head>

	<body>



		<!-- left navigation pane -->
		<?php include("travel-leftnav.inc.php"); ?>

		<!-- end left navigation rail -->

		<div class="col-md-9">
			<!-- start main content column -->
			<ol class="breadcrumb">
				<li><a href="#">Home</a></li>
				<li><a href="#">Browse</a></li>
				<li class="active">Posts</li>
			</ol>

			<div class="jumbotron" id="postJumbo">
				<h1>Posts</h1>
				<p>Read other travellers' posts ... or create your own.</p>
				<p><a class="btn btn-warning btn-lg">Learn more &raquo;</a></p>
			</div>

			<!-- start post summaries -->
			<div class="postlist">

				<!-- replace each of these rows with a function call -->

				<!-- replace pagination with function call -->
				<?php 
					outputPostRow(1);
					echo "<hr/>";
					outputPostRow(2);
					echo "<hr/>";
					outputPostRow(3);
		
					outputPagination(0, 4);
				?>



			</div>
			<!-- end main content column -->
		</div>
		<!-- end main content row -->
		</div>
		<!-- end main content container -->

		<!-- footer -->
		<?php include("travel-footer.inc.php") ?>


		<!-- Bootstrap core JavaScript
 ================================================== -->
		<!-- Placed at the end of the document so the pages load faster -->
		<script src="bootstrap3_travelTheme/assets/js/jquery.js"></script>
		<script src="bootstrap3_travelTheme/dist/js/bootstrap.min.js"></script>
		<script src="bootstrap3_travelTheme/assets/js/holder.js"></script>
	</body>

	</html>
